//
//  DateFormatter.swift
//  WeatherApp
//
//  Created by Mark Mokhles on 26/08/2022.
//  Copyright © 2022 Mark Mokhles. All rights reserved.
//

import Foundation

extension DateFormatter {
    enum Formats: String {
        case yyyyMMddTHHmmSSSSSZ = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        case yyyyMMddhhmma = "yyyy-MM-dd hh:mm a"
        case yyyyMMddTHHmmssZ = "yyyy-MM-dd'T'HH:mm:ssZ"
        case yyyyMMddhhmm = "yyyy-MM-dd HH:mm"
        case yyyyMMddhhmmss = "yyyy-MM-dd'T'HH:mm:ss"
        case yyyyMMddhhmmsshhmma = "yyyy-MM-dd'T'HH:mm:ss hh:mm a"
        case yyyyMMdd = "yyyy-MM-dd"
        case dMMM = "d MMM"
        case MMMM = "MMMM"
        case MMM = "MMM"
        case HHmmss = "HH:mm:ss"
        case HHmm = "HH:mm"
        case hhmma = "hh:mm a"
        case ddMMMyyyy = "dd MMM yyyy"
        case ddmmyyyy = "dd/MM/yyyy"
        case MMDDYY = "MM-dd-yyyy"
        case DDMMYY = "dd-MM-yyyy"
        case EEEEdMMMyyyy = "EEEE d MMM yyyy"
        case ddmmyyyyHHmmss = "dd/MM/yyyy HH:mm:ss"
        case dmmyyyy = "d MMM yyyy"
        case dMMyyyy = "d MMM. yyyy"
        case MMdyyyy = "MMM d,yyyy"
        case MMdyyyyy = "MMM d yyyy"
        case MMMMdyyyy = "MMMM d, yyyy"
        case dMMMMyyyy = "d MMMM yyyy"
        case dMMMMYYYYhhmma = "d MMMM yyyy, hh:mm a"
    }
    //"2022-08-26 16:23"

    func string(fromDate date: Date, withFormate format: Formats) -> String {
        self.dateFormat = format.rawValue
        return string(from: date)
    }

    func date(fromString string: String, withFormat format: Formats) -> Date? {
        self.dateFormat = format.rawValue
        return date(from: string)
    }
}
