//
//  UIView+Extension.swift
//  WeatherApp
//
//  Created by Mark Mokhles on 27/08/2022.
//  Copyright © 2022 Mark Mokhles. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    func setBorder(width: CGFloat = 1, color: UIColor, cornerRadius: CGFloat = 5) {
        layer.cornerRadius = cornerRadius
        layer.borderColor = color.cgColor
        layer.borderWidth = width
    }
}
