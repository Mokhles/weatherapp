//
//  AppConstant.swift
//  WeatherApp
//
//  Created by Mark Mokhles on 25/08/2022.
//  Copyright © 2022 Mark Mokhles. All rights reserved.
//

import Foundation


/// all requests key parameters
struct keyParameters {
    static var contentTypeKey = "Content-Type"
    static var languageKey = "Accept-Language"
    static var mobileNumber = "mobileNumber"
    static var countryCode  = "countryCode"
    static var smsCode = "smsCode"
    static var CountryCode = "CountryCode"
    static var MobileNumber = "MobileNumber"
    static var NewPassword = "NewPassword"
    static var SmsCode = "SmsCode"
    static var oldMobileNumber = "oldMobileNumber"
    static var oldCountryCode = "oldCountryCode"
    static var authorizationToken = "Authorization"
    static var days = "days"
    static var countryDetails = "q"
    static var key = "key"

    // News Request
    struct NewsSample {
        static var pageIndex = "pageIndex"
        static var pageSize = "pageSize"
    }
    struct EditProfile {
        static var isDeleted = "IsDeleteProfilePhoto"
        static var fullName = "FullName"
        static var gender = "Gender"
        static var birthdate = "Birthdate"
        static var file = "File"
        static var cityId = "CityId"
    }
}

struct keyParametersValues {
    static var contentTypeKey = "7e8dd443890c43fb9ee195840222508"
}

