//
//  APIFetcher.swift
//  WeatherApp
//
//  Created by Mark Mokhles on 25/08/2022.
//  Copyright © 2022 Mark Mokhles. All rights reserved.
//

import Foundation
import Alamofire
struct APIFetcher: Fetcher {
    @discardableResult
    func fetch<ResponseType>(request: BaseRequestProtocol, mappingInResponse responseType: ResponseType.Type, onSuccess: @escaping (ResponseType) -> Void, onFailure: @escaping (ServiceError) -> Void) -> DataRequest? where ResponseType: Codable {
        debugPrint("request url: \(request.url),request parameters: \(request.method), request header : \(request.headers), request parameters: \(request.parameters)")
        let dataRequest =
        SessionManager.default.request(request).validate().responseObject(responseType) { response in
            switch response.result {
            case .success(let result):
                onSuccess(result)
            case .failure(let error):
                onFailure(ServiceError(error: error))
            }
        }
        return dataRequest
    }
    
    static var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}

