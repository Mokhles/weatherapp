//
//  NetworkProtocols.swift
//  WeatherApp
//
//  Created by Mark Mokhles on 25/08/2022.
//  Copyright © 2022 Mark Mokhles. All rights reserved.
//

import Foundation
import Alamofire

protocol Fetcher {
    @discardableResult
    func fetch<ResponseType: BaseResponseProtocol> (request: BaseRequestProtocol, mappingInResponse response: ResponseType.Type, onSuccess: @escaping (ResponseType) -> Void, onFailure: @escaping (ServiceError) -> Void ) -> DataRequest?
}

protocol DataFetcher {
    var fetcher: Fetcher {get set}
}
