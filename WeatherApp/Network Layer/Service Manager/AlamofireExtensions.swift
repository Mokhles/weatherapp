//
//  AlamofireExtensions.swift
//  WeatherApp
//
//  Created by Mark Mokhles on 25/08/2022.
//  Copyright © 2022 Mark Mokhles. All rights reserved.
//

import Foundation
import Alamofire

extension SessionManager {

    func request(_ request: BaseRequestProtocol) -> DataRequest {
        return self.request(request.url,
                            method: request.method,
                            parameters: request.parameters,
                            encoding: request.method == .get ? URLEncoding.default : JSONEncoding.prettyPrinted,
                            headers: request.headers)
    }
}

extension DataRequest {

    @discardableResult
    func responseObject<T: Codable>(_: T.Type,
                                                 queue: DispatchQueue? = nil,
                                                 options: JSONSerialization.ReadingOptions = .allowFragments,
                                                 completionHandler: @escaping (Alamofire.DataResponse<T>) -> Void) -> Self {
        return responseJSON(queue: queue, options: options) { (response) in
            switch response.result {
            case .success:
                do {
                    if let JSONString = String(data: response.data!, encoding: String.Encoding.utf8) {
                        print(JSONString)
                    }
                    
                    let jsonDecoder = JSONDecoder()
                    let decodedObject = try jsonDecoder.decode(T.self, from: response.data ?? Data())

                    //logging only on the debug mode
                    print("WeatherAPP: API Call log: ***************************************************")
                    print(response.request?.url ?? "")
                    print(response.request?.allHTTPHeaderFields ?? "")
                    print(response.request?.httpBody ?? "")
                    print(response.request?.httpMethod ?? "")

                        if (response.response?.statusCode) == 200 {
                        completionHandler(DataResponse(request: response.request, response: response.response, data: response.data, result: .success(decodedObject), timeline: response.timeline))
                    } else {
                        let serverError = ResponseError(reason: "Some Thing Went Wrong")
                        completionHandler(DataResponse(request: response.request, response: response.response, data: response.data, result: .failure(serverError), timeline: response.timeline))
                        
                        
                    }
                } catch {
                    // Error Decoding the response
                    completionHandler(DataResponse(request: response.request, response: response.response, data: response.data, result: .failure(error), timeline: response.timeline))
                }
            case .failure(let error):
                // Error executing the request
                completionHandler(DataResponse(request: response.request, response: response.response, data: response.data, result: .failure(error), timeline: response.timeline))
            }
        }
    }
}
