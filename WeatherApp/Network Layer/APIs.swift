//
//  APIs.swift
//  WeatherApp
//
//  Created by Mark Mokhles on 25/08/2022.
//  Copyright © 2022 Mark Mokhles. All rights reserved.
//

import Foundation

class APIs {

    static private var kBaseURL: String {
      return "https://api.weatherapi.com/v1/"
    }

    //MARK:- APIs Class
    struct Forecast {
        static let basePath = "forecast.json?"
        static func getForecast() -> String {
            return kBaseURL + basePath
        }
    }

    struct Search {
        static let basePath = "search.json?"
        static func getCities() -> String {
            return kBaseURL + basePath
        }
    }
}
