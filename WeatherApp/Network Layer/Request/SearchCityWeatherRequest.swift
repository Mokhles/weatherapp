//
//  SearchCityWeatherRequest.swift
//  WeatherApp
//
//  Created by Mark Mokhles on 27/08/2022.
//  Copyright © 2022 Mark Mokhles. All rights reserved.
//

import Foundation
import Alamofire

class SearchCityWeatherRequest: BaseRequestProtocol {

    private var countryName: String

    init(countryName: String) {
        self.countryName = countryName
    }

    var url: String {
        return APIs.Search.getCities()
    }

    var method: HTTPMethod {
        return .get
    }

    var parameters: Parameters {
        return [keyParameters.key: keyParametersValues.contentTypeKey,
                keyParameters.countryDetails: countryName,
                keyParameters.days: 3
        ]
    }
}
