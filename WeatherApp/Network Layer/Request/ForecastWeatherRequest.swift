//
//  CurrentWeatherRequest.swift
//  WeatherApp
//
//  Created by Mark Mokhles on 26/08/2022.
//  Copyright © 2022 Mark Mokhles. All rights reserved.
//

import Foundation
import Alamofire

class ForecastWeatherRequest: BaseRequestProtocol {

    private var lat: String
    private var long: String
    private var cityName: String?

    init(longitude: String, latitude: String, cityName: String?) {

        self.lat = latitude
        self.long = longitude
        self.cityName = cityName
    }
    var url: String {
        return APIs.Forecast.getForecast()
    }

    var method: HTTPMethod {
        return .get
    }

    var parameters: Parameters {
        var countryDetails: String
        if cityName != "" {
            countryDetails = cityName ?? ""
        } else {
            countryDetails = "\(lat),\(long)"
        }
            return [keyParameters.key: keyParametersValues.contentTypeKey,
                    keyParameters.countryDetails: countryDetails as Any,
                    keyParameters.days: 3
                ]
        }
}
