//
//  Country.swift
//  WeatherApp
//
//  Created by Mark Mokhles on 27/08/2022.
//  Copyright © 2022 Mark Mokhles. All rights reserved.
//

import Foundation

struct Country: Codable {
    var id: Int?
    var name, region: String
    var country: String?
    var lat, lon: Double?
    var url: String?
}
