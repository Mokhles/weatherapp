//
//  ForecastWeather.swift
//  WeatherApp
//
//  Created by Mark Mokhles on 26/08/2022.
//  Copyright © 2022 Mark Mokhles. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct ForecastWeather: Codable {
    var location: Location?
    var current: Current?
    var forecast: Forecast?
}

// MARK: - Current
struct Current: Codable {
    var tempF: Double?
    var condition: Condition?
    var windMph, windKph: Double?
    var humidity: Int?

    enum CodingKeys: String, CodingKey {
        case tempF = "temp_f"
        case condition
        case windMph = "wind_mph"
        case windKph = "wind_kph"
        case humidity
    }
}

// MARK: - Condition
struct Condition: Codable {
    var text: String?
    var icon: String?
    var code: Int?
}


// MARK: - Forecast
struct Forecast: Codable {
    var forecastday: [Forecastday]?
}

// MARK: - Forecastday
struct Forecastday: Codable {
    var day: Day?

    enum CodingKeys: String, CodingKey {
        case day
    }
}


// MARK: - Day
struct Day: Codable {
    var avgtempC, avgtempF: Double?
    var condition: Condition?

    enum CodingKeys: String, CodingKey {
        case avgtempC = "avgtemp_c"
        case avgtempF = "avgtemp_f"
        case condition
    }
}


// MARK: - Location
struct Location: Codable {
    var country: String?
    var localtime: String?

    enum CodingKeys: String, CodingKey {
        case country
        case localtime
    }
}
