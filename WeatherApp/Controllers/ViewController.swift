//
//  ViewController.swift
//  WeatherApp
//
//  Created by Mark Mokhles on 25/08/2022.
//  Copyright © 2022 Mark Mokhles. All rights reserved.
//

import UIKit
import Kingfisher
import CoreLocation

class ViewController: BaseViewController, CLLocationManagerDelegate {

    // MARK: - iBoutlets

    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var lblCountry: UILabel!
    @IBOutlet private weak var lblDate: UILabel!
    @IBOutlet private weak var imgMainWeather: UIImageView!
    @IBOutlet private weak var containerViewImgMainWeather: UIView!
    @IBOutlet private weak var stackViewWind: UIStackView!
    @IBOutlet private weak var lblMainWeatherDegree: UILabel!
    @IBOutlet private weak var lblWind: UILabel!
    @IBOutlet private weak var lblHumidity: UILabel!
    @IBOutlet private weak var lblTodayWeatherDegree: UILabel!
    @IBOutlet private weak var imgTodayWeatherDegree: UIImageView!
    @IBOutlet private weak var lblTomorrowWeatherDegree: UILabel!
    @IBOutlet private weak var imgTomorrowWeatherDegree: UIImageView!
    @IBOutlet private weak var lblAfterTomorrowWeatherDegree: UILabel!
    @IBOutlet private weak var imgAfterTomorrowWeatherDegree: UIImageView!
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var viewContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var viewContainer: UIView!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var LblNoDataFound: UILabel!
    @IBOutlet private weak var buttonHeightConstraint: NSLayoutConstraint!

    // MARK: - iVars

    var locationManager = CLLocationManager()
    var currentLocatoin: CLLocation?
    var countryList = [Country]()

    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setupSearchBar()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupLocation()
    }

    // MARK: - locationManager

    func setupLocation(){
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !locations.isEmpty, currentLocatoin == nil {
            currentLocatoin = locations.first
            locationManager.stopUpdatingLocation()
            requsstWeatherForLocation()
        }
    }

    func requsstWeatherForLocation(){
        guard let currentLocatoin = currentLocatoin else {
            return
        }
        let log = currentLocatoin.coordinate.longitude
        let lat = currentLocatoin.coordinate.latitude
        getCurrentWeather(long: "\(log)", lat: "\(lat)", cityName: "")
    }

    // MARK: - SetupView

    func setUpView(){

        //TableView Delegate
        tableView.delegate = self
        tableView.dataSource = self

        //CornerViewContainer
        viewContainer.clipsToBounds = true
        viewContainer.layer.cornerRadius = 15
        viewContainer.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]

        //StackView
        stackView.setCustomSpacing(4, after: lblCountry)
        stackView.setCustomSpacing(25, after: containerViewImgMainWeather)
        stackView.setCustomSpacing(96, after: lblDate)
        stackView.setCustomSpacing(115, after: stackViewWind)
        searchBar.searchTextField.placeholder = "Search City"
        view.reloadInputViews()
    }

    // MARK: - Setup SearchBar

    private func setupSearchBar() {
        buttonHeightConstraint.constant = 0
        viewContainerHeightConstraint.constant = 130
        searchBar.searchTextField.font = UIFont.systemFont(ofSize: 15)
        searchBar.delegate = self
        searchBar.searchTextField.backgroundColor = .white
        searchBar.setBorder(color: .lightGray, cornerRadius: 25)
    }

    // MARK: - Target Actions

    @IBAction func backBtnisTapped(_ sender: Any) {
        clearSearchView()
    }

    @IBAction func searchBtnIsTapped(_ sender: Any) {
        viewContainer.isHidden = false
    }

    @IBAction func dismissViewBtnIsTapped(_ sender: Any) {
        clearSearchView()
    }

    // MARK: - Functions

    func getCurrentWeather(long: String, lat: String, cityName: String?) {
        showLoadingView()
        let request  = ForecastWeatherRequest(longitude: long, latitude: lat, cityName: cityName)
        APIFetcher().fetch(request: request, mappingInResponse: ForecastWeather.self, onSuccess: { [weak self]  response in

            self?.hideLoadingView()
            
            self?.setupData(weatherData: response)

        }) { [weak self] error in
            print(self.debugDescription)
        }
    }

    func getCountriesWeather(countryName: String) {
        showLoadingView()
        let request  = SearchCityWeatherRequest(countryName: countryName)
        APIFetcher().fetch(request: request, mappingInResponse: [Country].self, onSuccess: { [weak self]  response in

            self?.hideLoadingView()

            if response.isEmpty == true {
                self?.LblNoDataFound.isHidden = false
                self?.countryList.removeAll()
                self?.tableView.reloadData()
            } else {
                self?.LblNoDataFound.isHidden = true
                self?.countryList = response
                self?.tableView.reloadData()
            }

        }) { [weak self] error in
            print(self.debugDescription)
            print(error)
        }
    }

    func setupData(weatherData: ForecastWeather) {
        self.lblCountry.text = weatherData.location?.country

        let dateFormat = DateFormatter()
        if let date = weatherData.location?.localtime {
            if let date = dateFormat.date(fromString: date, withFormat: .yyyyMMddhhmm) {
                self.lblDate.text = dateFormat.string(fromDate: date, withFormate: .ddMMMyyyy)
            }
        }
        self.imgMainWeather.kf.setImage(with: URL(string: "https:\(weatherData.current?.condition?.icon ?? "")"))
        self.lblMainWeatherDegree.text = "\(weatherData.current?.tempF ?? 0.0)°F"
        self.lblWind.text = "\(weatherData.current?.windMph ?? 0.0) mph"
        self.lblHumidity.text = "\(weatherData.current?.humidity ?? 0)%"
        self.lblTodayWeatherDegree.text = "\(weatherData.forecast?.forecastday?[0].day?.avgtempC ?? 0.0)°" + "/" + "\(weatherData.forecast?.forecastday?[0].day?.avgtempF ?? 0.0)°F"
        self.imgTodayWeatherDegree.kf.setImage(with: URL(string: "https:\(weatherData.forecast?.forecastday?[0].day?.condition?.icon ?? "")"))

        self.lblTomorrowWeatherDegree.text = "\(weatherData.forecast?.forecastday?[1].day?.avgtempC ?? 0.0)°" + "/" + "\(weatherData.forecast?.forecastday?[1].day?.avgtempF ?? 0.0)°F"
        self.imgTomorrowWeatherDegree.kf.setImage(with: URL(string: "https:\(weatherData.forecast?.forecastday?[0].day?.condition?.icon ?? "")"))

        self.lblAfterTomorrowWeatherDegree.text = "\(weatherData.forecast?.forecastday?[2].day?.avgtempC ?? 0.0)°" + "/" + "\(weatherData.forecast?.forecastday?[2].day?.avgtempF ?? 0.0)°F"
        self.imgAfterTomorrowWeatherDegree.kf.setImage(with: URL(string: "https:\(weatherData.forecast?.forecastday?[0].day?.condition?.icon ?? "")"))
    }

    func clearSearchView() {
        searchBar.text = nil
        countryList.removeAll()
        tableView.reloadData()
        viewContainerHeightConstraint.constant = 130
        buttonHeightConstraint.constant = 0
        viewContainer.isHidden = true
        dismissKeyboard()
    }
}

// MARK: - UISearchBarDelegate

extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 2 {
            getCountriesWeather(countryName: searchText)
            buttonHeightConstraint.constant = 30
            viewContainerHeightConstraint.constant = 400
        } else {
            LblNoDataFound.isHidden = true
            buttonHeightConstraint.constant = 0
            viewContainerHeightConstraint.constant = 130
        }
    }
}

// MARK: - UITableView Data Source

extension ViewController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return countryList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath)
        cell.textLabel?.text = "\(countryList[indexPath.row].name) - \(countryList[indexPath.row].country ?? "")"
        return cell
    }
}

// MARK: - UITableView Delegate

extension ViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        getCurrentWeather(long: "", lat: "", cityName: countryList[indexPath.row].name)
        clearSearchView()
    }
}
