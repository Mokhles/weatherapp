//
//  BaseViewController.swift
//  WeatherApp
//
//  Created by Mark Mokhles on 27/08/2022.
//  Copyright © 2022 Mark Mokhles. All rights reserved.
//

import UIKit
import MBProgressHUD

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @discardableResult func showLoadingView(progressMode: MBProgressHUDMode = .indeterminate, title: String = "") -> MBProgressHUD {

        let progress = MBProgressHUD.showAdded(to: view, animated: true)
        // Configre hudProgress
        progress.mode = progressMode
        progress.animationType = .fade
        progress.label.text = "Loading"
        progress.progress = 0
        return progress
    }

    func hideLoadingView() {
        MBProgressHUD.hide(for: view, animated: true)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
